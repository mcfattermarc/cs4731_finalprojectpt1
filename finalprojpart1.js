var points;
var colors;
var theta = 0;

var NumVertices  = 36;

var gl;

var fovy = 45.0;  // Field-of-view in Y direction angle (in degrees)
var aspect;       // Viewport aspect ratio
var program;

var viewMatrix, mvMatrix, pMatrix;
var modelView;
const eye = vec3(0, 0, 10);
const at = vec3(0.0, 0.0, 0.0);
const up = vec3(0.0, 1.0, 0.0);


var stack = [];

function main()
{
    // Retrieve <canvas> element
    var canvas = document.getElementById('webgl');

    // Get the rendering context for WebGL
    gl = WebGLUtils.setupWebGL(canvas);

    //Check that the return value is not null.
    if (!gl)
    {
        console.log('Failed to get the rendering context for WebGL');
        return;
    }

    // Initialize shaders
    program = initShaders(gl, "vshader", "fshader");
    gl.useProgram(program);

    //Set up the viewport
    gl.viewport( 0, 0, 400, 400);

    aspect =  canvas.width/canvas.height;
    // Set clear color
    gl.clearColor(1.0, 1.0, 1.0, 1.0);

    // Clear <canvas> by clearing the color buffer
    gl.enable(gl.DEPTH_TEST);

    points = [];
    colors = [];

    //projection = gl.getUniformLocation(program, "projectionMatrix");
    modelView = gl.getUniformLocation(program, "modelMatrix");

    gl.clear( gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    render();
}

function cube()
{
    var verts = [];
    verts = verts.concat(quad( 1, 0, 3, 2 ));
    verts = verts.concat(quad( 2, 3, 7, 6 ));
    verts = verts.concat(quad( 3, 0, 4, 7 ));
    verts = verts.concat(quad( 6, 5, 1, 2 ));
    verts = verts.concat(quad( 4, 5, 6, 7 ));
    verts = verts.concat(quad( 5, 4, 0, 1 ));
    return verts;
}

function longCube()
{
    var verts = [];
    verts = verts.concat(longQuad( 1, 0, 3, 2 ));
    verts = verts.concat(longQuad( 2, 3, 7, 6 ));
    verts = verts.concat(longQuad( 3, 0, 4, 7 ));
    verts = verts.concat(longQuad( 6, 5, 1, 2 ));
    verts = verts.concat(longQuad( 4, 5, 6, 7 ));
    verts = verts.concat(longQuad( 5, 4, 0, 1 ));
    return verts;
}

function render()
{
    var redCube = cube();
    var blueCube = cube();
    var greenCube = longCube(); //the green will be the longer cube so as to balance things out
    var magentaCube = cube();
    var yellowCube = cube();
    var aquaCube = cube();

    //theta = theta + 0.15;

    pMatrix = perspective(fovy, aspect, .1, 100);

    //var projMatrix = gl.getUniformLocation(program, 'projMatrix');
    //gl.uniformMatrix4fv( projection, false, flatten(pMatrix) );


    viewMatrix = lookAt(eye, at , up);

    var projMatrix = gl.getUniformLocation(program, 'projMatrix');
    gl.uniformMatrix4fv(projMatrix, false, flatten(pMatrix));

    var viewMatrixLoc = gl.getUniformLocation(program, "viewMatrix");
    gl.uniformMatrix4fv(viewMatrixLoc, false, flatten(viewMatrix));

    mvMatrix = translate(0, 0, 0);

    //first, we start with the red on the top
    gl.uniformMatrix4fv(modelView, false, flatten(mvMatrix));
    draw(redCube, vec4(1.0, 0.0, 0.0, 1.0));
    /**
    stack.push(mvMatrix);
        mvMatrix = mult(mvMatrix, translate(0.0, 2.0, 0.0));

        gl.uniformMatrix4fv(modelView, false, flatten(mvMatrix));
        draw(redCube, vec4(1.0, 0.0, 0.0, 1.0));

        stack.push(mvMatrix); //this will be the yellow cube, directly under and to the right of the red cube
            mvMatrix = mult(mvMatrix, translate(1.25,-2,0));
            gl.uniformMatrix4fv(modelView, false, flatten(mvMatrix));
            draw(yellowCube, vec4(1.0, 1.0, 0.0, 1.0));

            stack.push(mvMatrix); //this will be the long green cube to balance the right side out
                mvMatrix = mult(mvMatrix, translate(0, -2, 0));
                gl.uniformMatrix4fv(modelView, false, flatten(mvMatrix));
                draw(greenCube, vec4(0.0, 1.0, 0.0));
            mvMatrix = stack.pop();

        mvMatrix = stack.pop();


        stack.push(mvMatrix); //the magenta cube, directly under and left of the red cube
            mvMatrix = mult(mvMatrix, translate(-1.25, -2, 0));
            gl.uniformMatrix4fv(modelView, false, flatten(mvMatrix));
            draw(magentaCube, vec4(1.0, 0.0, 1.0, 1.0));

            stack.push(mvMatrix); //the blue cube, underneath the magenta cube in front of it
                mvMatrix = mult(mvMatrix, translate(0, -2, 1.25));
                gl.uniformMatrix4fv(modelView, false, flatten(mvMatrix));
                draw(blueCube, vec4(0.0, 0.0, 1.0, 1.0));
            mvMatrix = stack.pop();

            stack.push(mvMatrix); //the aqua cube, underneath the magenta cube behind it
                mvMatrix = mult(mvMatrix, translate(0, -2, -1.25));
                gl.uniformMatrix4fv(modelView, false, flatten(mvMatrix));
                draw(aquaCube, vec4(0.0, 1.0, 1.0, 1.0));
            mvMatrix = stack.pop();

        mvMatrix = stack.pop();
    mvMatrix = stack.pop();
     */





/**
    stack.push(mvMatrix);
        mvMatrix = mult(rotateY(theta), mvMatrix);
        gl.uniformMatrix4fv( modelView, false, flatten(mvMatrix) );
        draw(redCube, vec4(1.0, 0.0, 0.0, 1.0));
        //mvMatrix = stack.pop();

        stack.push(mvMatrix);
            mvMatrix = mult(mvMatrix, translate(1, 1, 1));
            gl.uniformMatrix4fv( modelView, false, flatten(mvMatrix) );
            draw(magentaCube, vec4(1.0, 0.0, 1.0, 1.0));
        mvMatrix = stack.pop();
    mvMatrix = stack.pop();

    stack.push(mvMatrix);
    mvMatrix = mult(mvMatrix, translate(-1, -1, -1));
    gl.uniformMatrix4fv( modelView, false, flatten(mvMatrix) );
    draw(blueCube, vec4(0.0, 0.0, 1.0, 1.0));
    mvMatrix = stack.pop();

    gl.uniformMatrix4fv( modelView, false, flatten(mvMatrix) );
    draw(greenCube, vec4(0.0, 1.0, 0.0, 1.0));
 */

    //requestAnimationFrame(render); //required for animation


}

function draw(cube, color)
{
    console.log("oh boy we're drawing a cube");
    var fragColors = [];

    for(var i = 0; i < cube.length; i++)
    {
        fragColors.push(color);
    }

    var pBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, pBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(cube), gl.STATIC_DRAW);

    var vPosition = gl.getAttribLocation(program,  "vPosition");
    gl.vertexAttribPointer(vPosition, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vPosition);

    var cBuffer = gl.createBuffer();
    gl.bindBuffer(gl.ARRAY_BUFFER, cBuffer);
    gl.bufferData(gl.ARRAY_BUFFER, flatten(fragColors), gl.STATIC_DRAW);

    var vColor= gl.getAttribLocation(program,  "vColor");
    gl.vertexAttribPointer(vColor, 4, gl.FLOAT, false, 0, 0);
    gl.enableVertexAttribArray(vColor);

    gl.drawArrays( gl.TRIANGLES, 0, NumVertices );

}


function quad(a, b, c, d)
{
    var verts = [];

    var vertices = [
        vec4( -0.5, -0.5,  0.5, 1.0 ),
        vec4( -0.5,  0.5,  0.5, 1.0 ),
        vec4(  0.5,  0.5,  0.5, 1.0 ),
        vec4(  0.5, -0.5,  0.5, 1.0 ),
        vec4( -0.5, -0.5, -0.5, 1.0 ),
        vec4( -0.5,  0.5, -0.5, 1.0 ),
        vec4(  0.5,  0.5, -0.5, 1.0 ),
        vec4(  0.5, -0.5, -0.5, 1.0 )
    ];

    var indices = [ a, b, c, a, c, d ];

    for ( var i = 0; i < indices.length; ++i )
    {
        verts.push( vertices[indices[i]] );
    }

    return verts;
}

function longQuad(a, b, c, d)
{
    var verts = [];

    var vertices = [
        vec4( -0.5, -1.0,  0.5, 1.0 ),
        vec4( -0.5,  1.0,  0.5, 1.0 ),
        vec4(  0.5,  1.0,  0.5, 1.0 ),
        vec4(  0.5, -1.0,  0.5, 1.0 ),
        vec4( -0.5, -1.0, -0.5, 1.0 ),
        vec4( -0.5,  1.0, -0.5, 1.0 ),
        vec4(  0.5,  1.0, -0.5, 1.0 ),
        vec4(  0.5, -1.0, -0.5, 1.0 )
    ];

    var indices = [ a, b, c, a, c, d ];

    for ( var i = 0; i < indices.length; ++i )
    {
        verts.push( vertices[indices[i]] );
    }

    return verts;
}

